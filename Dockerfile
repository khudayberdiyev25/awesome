# workspace (GOPATH) configured at /go
FROM golang:1.18 as builder

WORKDIR /app

ADD . /app

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN CGO_ENABLED=0 GOARCH="amd64" GOOS=linux go build -ldflags="-s -w" -o ./bin/awesome main.go

FROM alpine:latest

COPY --from=builder app/bin/awesome ./awesome

RUN chmod +x ./awesome

CMD ["./awesome"]
